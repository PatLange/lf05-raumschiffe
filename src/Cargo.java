/** Represents a cargo.
 * @author Patrick Lange
*/
public class Cargo {
	
	/**The cargo's label.*/
	private String label;
	/**The cargo's amount.*/
	private int amount;
	
	/** Creates a cargo with the specified label and amount.
	 * @param label The cargo's label.
	 * @param amount The cargo's amount.
	*/
	public Cargo(String label, int amount) {
		this.label = label;
		this.amount = amount;
	}
	
	/** Gets the cargo's amount.
	 * @return An integer representing the amount of a cargo.
	*/
	public int getAmount() {
		return this.amount;
	}
	
	/** Sets the cargo's amount.
	 * @param amount The cargo's amount.
	*/
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	/** Gets the cargo's label.
	 * @return A string representing the label of a cargo.
	*/
	public String getLabel() {
		return this.label;
	}
	
	/** Sets the cargo's label.
	 * @param label The cargo's label.
	*/
	public void setLabel(String label) {
		this.label = label;
	}
}