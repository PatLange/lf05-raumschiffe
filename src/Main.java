/** This is the main class where initialization and execution takes place.
 * @author Patrick Lange
*/
public class Main {

	/**This is the main method which initializes all needed objects and makes use of other spaceship class' methods
	 * @param args Unused.
	*/
	public static void main(String[] args) {
//		Initialization
		
		//		Spaceships
		Spaceship klingons = new Spaceship("IKS Hegh'ta", 100, 100, 100, 100, 2, 1);
		Spaceship romulans = new Spaceship("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Spaceship vulcans = new Spaceship("Ni'Var", 80, 80, 100, 50, 5, 0);
		
		//		Cargo
		Cargo klingonCargo1 = new Cargo("Ferengi Snailjuice", 200);
		Cargo klingonCargo2 = new Cargo("Bat'leth Klingon Sword", 200);
		Cargo romulanCargo1 = new Cargo("Borgjunk", 5);
		Cargo romulanCargo2 = new Cargo("Red Matter", 2);
		Cargo romulanCargo3 = new Cargo("Plasma-Weapon", 50);
		Cargo vulcanCargo1 = new Cargo("Researchprobe", 35);
		Cargo vulcanCargo2 = new Cargo("Photontorpedo", 3);
		
		klingons.addCargo(klingonCargo1);
		klingons.addCargo(klingonCargo2);
		romulans.addCargo(romulanCargo1);
		romulans.addCargo(romulanCargo2);
		romulans.addCargo(romulanCargo3);
		vulcans.addCargo(vulcanCargo1);
		vulcans.addCargo(vulcanCargo2);
		
//		Execution
		klingons.shootTorpedo(romulans);
		romulans.shootPhaser(klingons);
		Spaceship.sendMessage("Violence is not logical");
		klingons.showShipStatus();
		klingons.showCargo();
		vulcans.repairShip(true, false, true, 5);
		vulcans.reloadTorpedo(3);
		vulcans.cleanupCargo();
		klingons.shootTorpedo(romulans);
		klingons.shootTorpedo(romulans);
		klingons.showShipStatus();
		klingons.showCargo();
		romulans.showShipStatus();
		romulans.showCargo();
		vulcans.showShipStatus();
		vulcans.showCargo();
	}
}
