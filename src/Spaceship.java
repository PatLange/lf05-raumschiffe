import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Represents a spaceship.
 * @author Patrick Lange
*/
public class Spaceship {

	/**The full name of a spaceship.*/
	private String name;
	/**The status of the energy supply of a spaceship.*/
	private int energySupply;
	/**The status of the shield of a spaceship.*/
	private int shield;
	/**The status of the life support systems of a spaceship.*/
	private int lifeSupportSystems;
	/**The status of the shell of a spaceship.*/
	private int shell;
	/**The amount of repair androis of a spaceship.*/
	private int repairAndroids;
	/**The amount of battle-ready photontorpedos of a spaceship.*/
	private int loadedPhotontorpedos;
	/**The log of all events.*/
	private static ArrayList<String> broadcastCommunicator = new ArrayList<String>();
	/**A list containing all cargo of a spaceship.*/
	private List<Cargo> cargoList;

	/** Creates a spaceship with the specified parameters and adds a new cargo list.
	 * @param name The spaceship's name.
	 * @param energySupply The spaceship's energy supply.
	 * @param shield The spaceship's shield.
	 * @param lifeSupportSystems The spaceship's life support systems.
	 * @param shell The spaceship's shell.
	 * @param repairAndroids The spaceship's repair androids.
	 * @param loadedPhotontorpedos The spaceship's combat-ready torpedos. 
	*/
	public Spaceship(String name, int energySupply, int shield, int lifeSupportSystems, int shell, int repairAndroids, int loadedPhotontorpedos) {
		this.name = name;
		this.energySupply = energySupply;
		this.shield = shield;
		this.lifeSupportSystems = lifeSupportSystems;
		this.shell = shell;
		this.repairAndroids = repairAndroids;
		this.loadedPhotontorpedos = loadedPhotontorpedos;
		this.cargoList = new ArrayList<Cargo>();

	}
	
	/** Gets the spaceship's name.
	 * @return A string representing the spaceship's name.
	*/
	public String getName() {
		return this.name;
	}

	/** Sets the spaceship's name.
	 * @param name The spaceship's name.
	*/
	public void setName(String name) {
		this.name = name;
	}

	/** Gets the spaceship's energy supply.
	 * @return An integer representing the spaceship's energy supply status.
	*/
	public int getEnergySupply() {
		return this.energySupply;
	}

	/** Sets the spaceship's energy supply status.
	 * @param energySupply The spaceship's energy supply.
	*/
	public void setEnergySupply(int energySupply) {
		this.energySupply = energySupply;
	}

	/** Gets the spaceship's shield.
	 * @return An integer representing the spaceship's shield status.
	*/
	public int getShield() {
		return this.shield;
	}

	/** Sets the spaceship's shield status.
	 * @param shield The spaceship's shield.
	*/
	public void setShield(int shield) {
		this.shield = shield;
	}

	/** Gets the spaceship's life support systems.
	 * @return An integer representing the spaceship's life support systems status.
	*/
	public int getLifeSupportSystems() {
		return this.lifeSupportSystems;
	}

	/** Sets the spaceship's life support systems status.
	 * @param lifeSupportSystems The spaceship's life support systems.
	*/
	public void setLifeSupportSystems(int lifeSupportSystems) {
		this.lifeSupportSystems = lifeSupportSystems;
	}

	/** Gets the spaceship's shell.
	 * @return An integer representing the spaceship's shell status.
	*/
	public int getShell() {
		return this.shell;
	}

	/** Sets the spaceship's shell status.
	 * @param shell The spaceship's shell.
	*/
	public void setShell(int shell) {
		this.shell = shell;
	}

	/** Gets the spaceship's repair androids.
	 * @return An integer representing the spaceship's amount of repair androids.
	*/
	public int getRepairAndroids() {
		return this.repairAndroids;
	}

	/** Sets the spaceship's amount of repair androids.
	 * @param repairAndroids The spaceship's repair androids.
	*/
	public void setRepairAndroids(int repairAndroids) {
		this.repairAndroids = repairAndroids;
	}

	/** Gets the spaceship's loaded photontorpedos.
	 * @return An integer representing the spaceship's amount of battle-ready torpedos.
	*/
	public int getLoadedPhotontorpedos() {
		return loadedPhotontorpedos;
	}

	/** Sets the spaceship's loaded photontorpedos.
	 * @param loadedPhotontorpedos The spaceship's battle-ready photontorpedos.
	*/
	public void setLoadedphotontorpedos(int loadedPhotontorpedos) {
		this.loadedPhotontorpedos = loadedPhotontorpedos;
	}

	/** Gets the broadcastCommunicator.
	 * @return An array list containing strings of all major events.
	*/
	public static ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	/** Sets the broadcast communicator.
	 * @param newBroadcastCommunicator The broadcast communicator.
	*/
	public static void setBroadcastCommunicator(ArrayList<String> newBroadcastCommunicator) {
		broadcastCommunicator = newBroadcastCommunicator;
	}

	/** Shoots a torpedo if available, sends a success message and calls the method {@link #spaceshipHit}.
	 * 	If 0 torpedos are loaded a corresponding message is sent.
	 * @param target An object representing the targeted spaceship.
	*/
	public void shootTorpedo(Spaceship target) {
		if (this.loadedPhotontorpedos > 0) {
			this.loadedPhotontorpedos -= 1;
			sendMessage("Photontorpedo launched");
			spaceshipHit(target);
		} else {
			sendMessage("-=*click*=-");
		}
	}

	/** Shoots a phaser if enough energy is available, sends a success message and calls the method {@link #spaceshipHit}. 
	 * 	If the energy supply is insufficient a corresponding message is sent.
	 * @param target An object representing the targeted spaceship.
	*/
	public void shootPhaser(Spaceship target) {
		if (this.energySupply < 50) {
			sendMessage("-=*click*=-");
		} else {
			sendMessage("Phasercannon launched");
			this.energySupply -= 50;
			spaceshipHit(target);
		}
	}

	/** Calculates the damage taken by a ship after being shot at and prints a corresponding message. 
	 * @param Spaceship An object representing the hit spaceship.
	*/
	private void spaceshipHit(Spaceship Spaceship) {
		System.out.println(Spaceship.name + " hit");
		Spaceship.shield -= 50;
		if (Spaceship.shield <= 0) {
			Spaceship.shell -= 50;
			Spaceship.lifeSupportSystems -= 50;
		}
		if (Spaceship.shield <= 0) {
			this.setLifeSupportSystems(0);
			sendMessage("Life Support Systems destroyed");
		}
	}

	/** Sends a message to everyone and adds it to the {@link #broadcastCommunicator}.
	 * @param message A string containing the message to be sent.
	*/
	public static void sendMessage(String message) {
		System.out.println(message);
		broadcastCommunicator.add(message);
	}

	/** Adds a new Cargo Object to the cargoList of a spaceship.
	 * @param newCargo An object representing the new cargo to add.
	*/
	public void addCargo(Cargo newCargo) {
		cargoList.add(newCargo);
	}

	/** Prints the ship's log.
	*/
	public static void returnLogfile() {
		System.out.println(getBroadcastCommunicator());
	}

	/** Loads photontorpedos into the ship if available and prints a success message.
	 *  If there are not enough photontorpedos available a corresponding message is printed. 
	 * @param photontorpedos An integer representing the amount of torpedos to be loaded into the spaceship.
	*/
	public void reloadTorpedo(int photontorpedos) {
		Cargo torpedoCargo = cargoList.stream().filter(cargo -> {
			return cargo.getLabel().equals("Photontorpedo");
		}).findFirst().get();

		if (torpedoCargo.getAmount() == 0 || torpedoCargo == null) {
			System.out.println("No photontorpedos found!");
			sendMessage("-=*click*=-");
		}
		else if (photontorpedos > torpedoCargo.getAmount()) {
			photontorpedos = torpedoCargo.getAmount();
		}
		torpedoCargo.setAmount(torpedoCargo.getAmount() - photontorpedos);
		this.loadedPhotontorpedos += photontorpedos;
		System.out.println(photontorpedos + " photontorpedo(s) reloaded");

	}

	/** Calculates the amount of repairs a spaceship gets from using repair androids.
	 * @param repairShield A boolean that represent if the shield needs repairs.
	 * @param repairLifeSupportSystems A boolean that represent if the life support systems need repairs.
	 * @param repairShell A boolean that represent if the shell needs repairs.
	 * @param usedRepairAndroids An integer representing the amount of repair androids used for the repairs.
	*/
	public void repairShip(boolean repairShield, boolean repairLifeSupportSystems, boolean repairShell, int usedRepairAndroids) {
		int damagedStructures = 0;
		
		if (repairShield) {
			damagedStructures += 1;
		}
		if (repairLifeSupportSystems) {
			damagedStructures += 1;		
		}
		if (repairShell) {
			damagedStructures += 1;
		}
		
		if (usedRepairAndroids > this.repairAndroids) {
			usedRepairAndroids = this.repairAndroids;
		}
		
		int repairAmount = ((int)(Math.random() * ((99 - 1) + 1) + 1) * usedRepairAndroids) / damagedStructures;
		
		if (repairShield) {
			this.shield += repairAmount;
		}
		if (repairLifeSupportSystems) {
			this.lifeSupportSystems += repairAmount;
		}
		if (repairShell) {
			this.shell += repairAmount;
		}
	}

	/** Prints the status of the spaceship.
	*/
	public void showShipStatus() {
		System.out.println(
				"Name: " + getName() + "\n" + 
				"Energy Supply: " + getEnergySupply() + "\n" + 
				"Shield: " + getShield() + "\n" + 
				"Life Support Systems: " + getLifeSupportSystems() + "\n" + 
				"Shell: " + getShell() + "\n" + 
				"Repair Androids: " + getRepairAndroids() + "\n" + 
				"Photontorpedos loaded: "+ getLoadedPhotontorpedos());
	}

	/** Prints the cargo of the spaceship.
	*/
	public void showCargo() {
		for (Cargo cargo : cargoList) {
			System.out.println("Label: " + cargo.getLabel() + "\n" + "Amount: " + cargo.getAmount());
		}
	}

	/** Cleans up the cargo of the spaceship by removing empty cargo from the spaceship's cargo list.
	*/
	public void cleanupCargo() {
		List<Cargo> filteredCargoList = cargoList.stream().filter(cargo -> {
			return cargo.getAmount() > 0;
		}).collect(Collectors.toList());
		
		this.cargoList = filteredCargoList;
	}
}
